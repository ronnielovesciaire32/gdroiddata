#!/bin/bash
#python env/get_package_list.py metadata/index-v1.json | sort -u > metadata/packages.txt

# first fun took 44 minutes

# run N in parallel
N=20

result_file=metadata/m_github_stars.yaml

rm -f $result_file ; touch $result_file
rm -f /tmp/retry.txt ; touch /tmp/retry.txt

env/get_package_list.py metadata/index-v1.json --github-hosted | sort -u > /tmp/github_packages.txt

task(){
        sleep 0.5;
	p=$1
        echo processing $p  ...
        srcurl=`env/get_package_list.py metadata/index-v1.json --get-app $p sourceCode`
        echo source URL is $srcurl
        stars=`curl -sqL $srcurl | grep starred | egrep -o '[0-9]*'| head -n1`
        echo found $stars stars
        res=`echo $p: $stars`
        echo result is: $res
	if [ -z "$stars" ]
	then
		echo has to retry ... sleeping to give github a break
	        echo $p >> /tmp/retry.txt
		# 15 min mit sl30 , 7min it sl2
		sleep 2
		exit 0
	fi
        echo storing result in $result_file
        echo $res >> $result_file
}


# main proc starts here

all=`wc -l /tmp/github_packages.txt | egrep -o "[0-9]*"`
for pp in `cat /tmp/github_packages.txt`
do
	echo task: $i of $all
	((i=i%N)); ((i++==0)) && wait
	task "$pp" &
done
wait

# quick exit for slow worker
exit 0 

# retry less parallel
N=5
sleep 2

cp /tmp/retry.txt /tmp/github_packages.txt
all=`wc -l /tmp/github_packages.txt | egrep -o "[0-9]*"`
rm -f /tmp/retry.txt ; touch /tmp/retry.txt

for pp in `cat /tmp/github_packages.txt`
do
	echo task: $i of $all
	((i=i%N)); ((i++==0)) && wait
	task "$pp" &
done
wait


# retry less parallel
N=2
sleep 2

cp /tmp/retry.txt /tmp/github_packages.txt
all=`wc -l /tmp/github_packages.txt | egrep -o "[0-9]*"`
rm -f /tmp/retry.txt ; touch /tmp/retry.txt

for pp in `cat /tmp/github_packages.txt`
do
	echo task: $i of $all
	((i=i%N)); ((i++==0)) && wait
	task "$pp" &
done
wait


# retry not parallel
sleep 2

cp /tmp/retry.txt /tmp/github_packages.txt
all=`wc -l /tmp/github_packages.txt | egrep -o "[0-9]*"`
rm -f /tmp/retry.txt ; touch /tmp/retry.txt

for pp in `cat /tmp/github_packages.txt`
do
	echo task: $i of $all
	task "$pp"
	sleep 1
done


