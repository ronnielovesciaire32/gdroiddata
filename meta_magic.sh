#!/bin/bash
#python env/get_package_list.py metadata/index-v1.json | sort -u > metadata/packages.txt

# get similarities
git clone https://gitlab.com/gdroid/app_match.r.git
cp app_match.r/app_match.txt ./metadata/app_match.yaml
rm -rf app_match.r

. env/bin/activate
env/process_meta_metric.py || exit 1
echo zipping the jar file
zip -9 metadata/gdroid.jar metadata/gdroid.json
#find .
deactivate

